yieldUnescaped '<!DOCTYPE html>'
html(lang:'en'){

    head {
        include template: "header.tpl"
    }

    body(class:"antialiased", onload:"prettyPrint();") {

        include template: 'menu.tpl'

        div(id:"top", class:"container"){
          bodyContents()
        }
        newLine()
        include template: 'footer.tpl'


        script(src:"${config.site_contextPath}js/vendor/jquery.js"){}
        newLine()
        /*script(src:"${config.site_contextPath}js/foundation.min.js"){}*/
        script(src:"${config.site_contextPath}js/vendor/bootstrap.min.js"){}
        newLine()        
        script(src:"${config.site_contextPath}js/vendor/prettify.js"){}
        newLine()
        script(src:"${config.site_contextPath}js/app.js"){} newLine()
        script(src:"${config.site_contextPath}lunr.js"){} newLine()

        script(defer:true, "data-project":"668bd32d6ecd27bb3a04e011", src:"https://cdn.jsdelivr.net/gh/litlyx/litlyx-js/browser/litlyx.js"){} newLine()
    }
}
newLine()
