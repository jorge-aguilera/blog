div(class:"row"){
    div(class:"col-md-12 content-card"){
        h1{ a(href:"${config.site_contextPath}${video.uri}","${video.title}") }
        ul(class:"list-inline"){
            li{ i(class:"fa fa-calendar"){ yield " ${video.date.format('MMMM, dd yyyy')}"} }
            li{ i(class:"fa fa-user"){ yield " ${video.author ?: config.site_author}"} }
        }
        div(class:"row"){
            p(class:"summary", "${video.summary ?: ''}")
            div(class:"text-right"){
                a(class:"btn btn-custom", href:"${content.rootpath}${video.noExtensionUri ?: video.uri}", role:"button","Ver")
            }
        }
    }
}