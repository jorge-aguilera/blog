div(class:"row"){
    div(class:"col-md-12 content-card"){        
        h1{ a(href:"${config.site_contextPath}${post.uri}","${post.title}") }
        ul(class:"list-inline"){            
            li{ i(class:"fa fa-calendar"){ yield " ${post.date.format('MMMM, dd yyyy')}"} }                       
            li{ i(class:"fa fa-user"){ yield " ${post.author ?: config.site_author}"} }
            li{ i(class:"fa fa-clock-o"){ yield " $post.read_time"} }
            include template: 'tags-brick.tpl'
        }        
        div(class:"row"){
            p(class:"summary", "${post.summary ?: ''}")
            div(class:"text-right"){ 
                a(class:"btn btn-custom", href:"${content.rootpath}${post.noExtensionUri ?: post.uri}", role:"button","Read More")
            }
        }        
    }
}