div(class:"row"){
    div(class:"col-md-12 content-card"){
        h1{ yield "${post.title}" }
        ul(class:"list-inline"){
            li{ i(class:"fa fa-calendar"){ yield " ${post.date.format('MMMM, dd yyyy')}"} }
            li{ i(class:"fa fa-user"){ yield " ${post.author ?: config.site_author}"} }
            include template: 'tags-brick.tpl'
        }
                        
        yieldUnescaped post.body
        div(class:"paragraph"){
            "video-js"(id:"vid1", class:"video-js vjs-fluid"){
                source(src:"https://cdn.jorge-aguilera.blog/${post.video}")
            }
        }
    }
}

div(clas:"row"){
    div(class:"col-md-12 content-card"){
        h2{yield "Este texto ha sido escrito por un humano"}
        h2{yield "This post was written by a human"}
    }
}