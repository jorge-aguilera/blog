layout 'layout/main.tpl', true,
        projects: projects,
        bodyContents: contents {

            div(class:"row", id:"content-main"){
		        div(class:"col-md-8"){

                    published_snippets.sort{it.date}.reverse().each { snippet ->
                        model.put('snippet', snippet)
                        include template: 'snippet-brick.tpl'
                    }
                    include template: "prev-next.tpl"
                }                
                div(class:"col-md-4"){
                    include template: "sidebar.tpl"
                }
            }
            div(class:"row"){
                div(class:"col-md-8"){
                    hr()
                    yield "Older post are available in the "
                    a(href:"${config.site_contextPath}${config.archive_file}","archive")
                }
            }

        }
