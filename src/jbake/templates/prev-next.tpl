if (config.index_paginate && numberOfPages > 1){
    div(class:"row"){
        div(class:"col-md-12"){
            div(class:"row"){
                div(class:"col-md-4", style:"text-align: left; padding-left: 0px"){
                    a(class:"${ currentPageNumber==1 ? 'disabled' : ''} btn btn-default", href:"${content.rootpath}${currentPageNumber==2?'':currentPageNumber-1}", 
                    	role:"button", style:"border-radius: 0px; display: block"){ 
                            i(class:"fa fa-arrow-circle-left", "aria-hidden":"true"){ yield ''} 
                        }
                }
                div(class:"col-md-4", style:"text-align: center; padding-right: 0px"){
                	a(class:"btn btn-default disabled", style="border-radius: 0px; display: block", "${currentPageNumber}/${numberOfPages}")
	            }
                div(class:"col-md-4", style:"text-align: left; padding-left: 0px"){
                    a(class:"${ currentPageNumber < numberOfPages ? '' : 'disabled'} btn btn-default", href:"${content.rootpath}${currentPageNumber + 1}", 
                    	role:"button", style:"border-radius: 0px; display: block"){ 
                            i(class:"fa fa-arrow-circle-right", "aria-hidden":"true"){ yield ''} 
                        }
                }
            }
        }
    }
}