layout 'layout/main.tpl', true,
        projects: projects,
        bodyContents: contents {

            div(class:"row", id:"content-main"){
		        div(class:"col-md-8"){
                    published_posts.each { post ->
                        model.put('post', post)
                        include template: 'post-brick.tpl'
                    }
                    include template: "prev-next.tpl"
                }                
                div(class:"col-md-4"){
                    include template: "sidebar.tpl"
                }
            }
            div(class:"row"){
                div(class:"col-md-8"){
                    hr()
                    yield "Older post are available in the "
                    a(href:"${config.site_contextPath}${config.archive_file}","archive")
                }
            }

        }
