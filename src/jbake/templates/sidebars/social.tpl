div(class:'demopadding'){
	if (config.sidebar_social_github){
		div(class:"icon social gh"){ 
            a(href:"//gitlab.com/${config.sidebar_social_github}",target:"_blank", title:"Github"){ 
                i(class:"fa fa-github"){} 
            } 
        }
	}
    if (config.sidebar_social_twitter){
		div(class:"icon social tw"){ 
            a(href:"//twitter.com/${config.sidebar_social_twitter}",target:"_blank", title:"Twitter"){ 
                i(class:"fa fa-twitter"){} 
            } 
        }
	}
    if (config.sidebar_social_linkedin){
		div(class:"icon social in"){ 
            a(href:"//linkedin.com/in/${config.sidebar_social_linkedin}",target:"_blank", title:"Linkedin"){
                i(class:"fa fa-linkedin"){} 
            } 
        }
	}
	if (config.sidebar_social_telegram){
        div(class:"icon social telegram"){
            a(href:"${config.sidebar_social_telegram}",target:"_blank", title:"Telegram"){
                i(class:"fa fa-telegram"){}
            }
        }
    }
    if (config.sidebar_social_mastodon){
        div(class:"icon social mastodon"){
            a(rel:"me", href:"${config.sidebar_social_mastodon}",target:"_blank", title:"Mastodon"){
                i(class:"fa fa-mastodon"){}
            }
        }
    }
}
