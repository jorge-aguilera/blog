div(class:"row"){
	div(class:"col-md-11 col-md-offset-1 card intro"){
		div(class:"logo"){
			a(href:"${config.sidebar_intro_about}"){ 
                img(src:"${config.sidebar_intro_pic_src}", 
					alt="${config.sidebar_intro_header}",
				    class:"img-responsive img-circle", style:"margin: 0 auto;") 
            }
		}
		br()
		div(class:"col-md-12 text-center"){
			div(class:"header"){
				a(href:"${config.sidebar_intro_about}"){ yield "${config.sidebar_intro_header}"}
			}
			div(class:"summary"){
				p{ yield "${config.sidebar_intro_summary}"}
				include template: "sidebars/social.tpl"
			}			
		}
	}	
}