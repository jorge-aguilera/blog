div(class:"row"){
    div(class:"col-md-11 col-md-offset-1 content-card card"){
        h5("Tags")
        ul(class:"list-inline tags", style="margin-top: 15px; margin-left: 0px"){
            alltags.each{ tag->
                li(style:""){ a(href:"/tags/${tag}${config.output_extension}","${tag}") }
            }
        }
    }
}
        

