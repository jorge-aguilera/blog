div(class:"row"){
    div(class:"col-md-12 content-card"){
        h1{ yield "${post.title}" }
        ul(class:"list-inline"){
            li{ i(class:"fa fa-calendar"){ yield " ${post.date.format('MMMM, dd yyyy')}"} }
            li{ i(class:"fa fa-user"){ yield " ${post.author ?: config.site_author}"} }
            li{ i(class:"fa fa-clock-o"){ yield " $post.read_time"} }
            include template: 'tags-brick.tpl'
        }
                        
        yieldUnescaped post.body       
    }
}
div(clas:"row"){
    div(class:"col-md-12 content-card"){
        h2{yield "Este texto ha sido escrito por un humano"}
        h2{yield "This post was written by a human"}
    }
}