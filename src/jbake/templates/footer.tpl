
div(class:"row"){
    div(class:"small-12 small-text-center"){
        p(class:"muted credit"){
         yield "2019 - ${new Date().format("yyyy")} | "
         yield "Mixed with "
         /*a(href:"http://foundation.zurb.com/","Foundation v${config.foundation_version}")*/
         a(href:"http://getbootstrap.com/","Bootstrap")
         yield " | Baked with "
         a(href:"http://jbake.org","JBake ${version}")
         yield " | Terminos "
         a(href:"/terminos.html","Terminos")
         yield " y "
         a(href:"/politica.html","Privacidad")
        }
    }
}