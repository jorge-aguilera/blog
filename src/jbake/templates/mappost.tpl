model.put("projects",projects)
layout 'layout/main.tpl', true,
        bodyContents: contents {
            model.put('post', content)
            include template: 'content-single.tpl'
            if (config.sidebar_social_telegram){
                div(class:"row"){
                    div(class:"col-md-12 content-card"){
                        span{ yield "Follow comments at " }
                        a(href:"${config.sidebar_social_telegram}",target:"_blank", title:"Telegram"){ yield "Telegram group" }
                        span{ yield " Or subscribe to the Channel " }
                        a(href:"${config.sidebar_social_telegram_channel}",target:"_blank", title:"Telegram"){ yield "Telegram channel" }
                    }
                }
            }
        }
