meta(charset:"utf-8") newLine()
meta(name:"viewport", content:"width=device-width, initial-scale=1.0") newLine()
title("${content.title ?: config.blog_title}") newLine()

link(rel:"stylesheet", href:"${config.site_contextPath}css/bootstrap.min.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/highlightjs-themes/androidstudio.css") newLine()

link(rel:"stylesheet", href:"${config.site_contextPath}css/asciidoctor.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/foundation.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/social_foundicons.css") newLine()

link(rel:"stylesheet", href:"${config.site_contextPath}css/phlat.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/phlat-custom.css") newLine()

link(rel:"stylesheet", href:"${config.site_contextPath}css/prettify.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/desert.css") newLine()
link(rel:"stylesheet", href:"//use.fontawesome.com/releases/v5.15.4/css/all.css") newLine()
link(rel:"stylesheet", href:"//use.fontawesome.com/releases/v5.15.4/css/v4-shims.css") newLine()
link(rel:"stylesheet", href:"${config.site_contextPath}css/app.css") newLine()

link(rel:"stylesheet", href:"//vjs.zencdn.net/8.16.1/video-js.css") newLine()

link(rel:"me", href:"https://jvm.social/@jorge") newLine()
link(rel:"me", href:"https://social.aguilera.soy/@jorge") newLine()

newLine()
script(src:"${config.site_contextPath}js/vendor/modernizr.js"){} newLine()
script(src:"https://unpkg.com/lunr/lunr.js"){} newLine()
