nav(class:"navbar navbar-default navbar-fixed-top"){
    div(class:"container"){
        div(class:"navbar-header"){
            button(type:"button", class:"navbar-toggle collapsed", "data-toggle":"collapse", "data-target":"#navbar", "aria-expanded":"false", "aria-controls":"navbar"){
                span(class:"sr-only"){ yield "Toggle navigation"}
                span(class:"icon-bar"){}
                span(class:"icon-bar"){}
                span(class:"icon-bar"){}
            }
            a(class:"navbar-brand", href:"/","Soy Jorge Aguilera")
        }
        div(id:"navbar", class:"navbar-collapse collapse"){
            ul(class:"nav navbar-nav navbar-right"){
                li{
                    a(href:"${config.site_contextPath}index.html", "Home"){ i(class:"fa fa-home"){yieldUnescaped '&nbsp;'} }
                }
                li{
                    a(href:"${config.site_contextPath}videos.html", "Charlas"){ i(class:"fa fa-video"){yieldUnescaped '&nbsp;'} }
                }
                li{
                    a(href:"${config.site_contextPath}${config.archive_file}","Archive"){ i(class:"fa fa-archive"){yieldUnescaped '&nbsp;'} }
                }
                li{
                    a(href:"${config.site_contextPath}${config.feed_file}","Feed"){ i(class:"fa fa-rss"){yieldUnescaped '&nbsp;'} }
                }
            }
            ul(class:"nav navbar-nav navbar-right"){
                li{
                    form(class:"navbar-form navbar-right"){
                        div(class:"form-group"){
                            i(class:"fa fa-search"){yieldUnescaped '&nbsp;'}
                            input(id:"search", type:"search", name:"q", placeholder:"Buscar...", maxlength:"80", autocomplete:"off", class:"form-control")                            
                            ul(class:"searchresults")
                        }
                    }
                }                
            }
        }
    }
}
