div(class:"row"){
    div(class:"col-md-12 content-card"){        
        h3("${snippet.title}")
        div(class:"row"){
            p(class:"summary", "${snippet.body ?: ''}")            
        }        
    }
}