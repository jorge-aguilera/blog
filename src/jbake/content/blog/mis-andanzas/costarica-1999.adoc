= Costa Rica 1999
Jorge Aguilera
2023-09-23
:jbake-type: mappost
:jbake-status: published
:jbake-tags: personales, viajes
:jbake-summary: Primer viaje transoceánico
:jbake-coordinates: -83.712266,8.6543759
:jbake-map: mis-andanzas
:idprefix:
:icons: font

A poco que hayas hablado conmigo seguro que me has tenido que aguantar la chapa sobre
mi primer viaje a Costa Rica y de cómo me marcó

Para entonces yo cumplía los 30 y para ser sincero no había salido mucho fuera de España. Lo típico, alguna escapada
a Londres unos días, una semana santa a Viena ... Pero lo de viajar me ponía en un estado de mal humor, nervioso
y me cerraba en la típica frase de mi viejo "qué se me ha perdido a mí allí"

La suerte, y mi mentor, me llevó a hacer mi primer viaje transoceánico y algo hizo "clic" y lo cambió todo.
A partir de entonces me propuse, en la medida de lo posible, hacer al menos un viaje al año a tierras
lo más lejanas a España

== Para recordar

- El viaje a Tortugero en barca. Mi primer aligator
- El rugir del volcán Arenal en mis pies (unos meses después entró en erupción)
- A Javi (jacalla) y Manu el granadino. Ande estarán?
- El resort PoorMan en Corcovado. 2 días se tardaba en llegar

== De todo se aprende

- Antes de que el avión despegara ya había perdido todo el dinero que llevaba (me dejé la mochila abierta
con los dólares a la vista)
- Llegamos un viernes por la tarde y como había pagado los billetes con la tarjeta 4b ... no pude sacar dinero
- Perdí las llaves de la casa de mis colegas en una cabaña de Tortugero
- Si te dicen "me regala la maleta" mirar que es el conductor guardandolas en la bodega del bus

