
This is the {part} part of a series of posts about how I'll develop an application in Kubernetes (k8s)

- first post: Idea (https://jorge-aguilera.blog/blog/prestamos-bibliotecas/k8s-1.html)
- second post: Infraestructure (https://jorge-aguilera.blog/blog/prestamos-bibliotecas/k8s-2.html)
- third post: Job (https://jorge-aguilera.blog/blog/prestamos-bibliotecas/k8s-3.html)


The main ot these posts is to document the process of deploying a solution in k8s at the same
time I'm writting the application so probably all posts will have a lot of errors and mistakes that I need to
correct in the next post.

NOTE: Be aware that I'm a very nobel with Kubernetes and these are my first steps with it.
I hope to catch up the attention of people with more knowledge than me and maybe they can review these posts and suggest
to us some improvements.

TIP: I've created a git repository at https://gitlab.com/puravida-software/k8s-bibliomadrid
with the code of the application