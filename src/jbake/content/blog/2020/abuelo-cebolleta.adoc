= Abuelo cebolleta
Jorge Aguilera
2020-06-27
:jbake-type: post
:jbake-status: published
:jbake-tags: pensamientos, introspección
:jbake-summary: Apuntes tomados para la entrevista en Xataka "20 años después quiero volver a programar: qué grandes cambios ha habido y por dónde empezar"
:idprefix:

image::2020/abuelo_cebolleta.jpg[]

Hace unas semanas contactaron conmigo por si quería colaborar para un artículo en Xataka dando mi opinión como
programador viejuno sobre como ha cambiado en todos estos años la industria del software y dando algún consejo a alguien
que abandonara hace 20 años la programación y quisiera volver.

Como el artículo incluía a otros programadores recojo aquí alguna de mis respuestas, añadiendo algunos comentarios que
por no venir a cuento no incluí en su momento.

NOTE:: El artículo en cuestión lo puedes leer en
https://www.xataka.com/otros/20-anos-despues-quiero-volver-a-programar-que-grandes-cambios-ha-habido-donde-empezar junto
con las opiniones de otros 3 programadores

== Presentación

Soy Jorge Aguilera, un Senior Software Engineer, actualmente trabajando en Tymit, con unos
25 años trabajando en el sector del desarrollo de software.

(_Repasando luego mi vida laboral descubrí que el mes que viene hare 28 años_)

Como muchos de mi generación mi adolescencia se vio marcada por la aparición de aquellas
consolas que te dejaban hacer alguna cosa más que las máquinas de marcianos y siempre sentí
atracción por ellas. Pero cuando decidí dar el salto fue
cuando empezamos a usar Multiplan (una hoja de cálculo) en la carrera de Económicas, y ví
que me saltaba las clases de todas las asignaturas por estar diseñando funciones estadísticas en el aula de informática.

== Inicios

Gracias a un amigo ( _ Merchán, @jmerespi, amigo desde el colegio y al que le debo un huevo de cosas_)
me apunté a un programa de becas de aquella época: 3 meses a media jornada y hasta me pagaban!!!

(_La beca fue de
3 meses durante el verano y la compaginé con un trabajo de vigilante los fines de semana, así que esos meses gané una
pasta gansa, unas 40.000 pesetas al mes. Recuerdo que eramos unos
20 y de muy diversos perfiles y allí conocí a quien sería mi mentor durante muchos años_)

Por aquellos tiempos, y creo que todavía de alguna manera en estos, la evolución normal era que
estuvieras unos años de junior, otros de senior, analista, jefe de proyecto ... y debo admitir
que en aquellos días lo veía como la evolución normal. De hecho llegué a montar una empresa donde
ejercí como director técnico.

(_Releo esto y veo que no venía muy a cuento pero tenía que decirlo_)

Por suerte y gracias a buenos consejeros opté hace muchos años por la vía "arriesgada" y decidí dedicarme al desarrollo
como tal y desde entonces no me arrepiento ni me veo cambiando el rumbo.

(_Ahora veo que esto era el meollo de la cuestión:
cómo por buenos consejos, suerte e introspección conseguí escapar de la corriente del momento y dedicarme
únicamente a programar que era lo que me gustaba_)

== Trayectoria

Echando la vista atrás veo que mientras antes podías abarcar prácticamente todo el espectro de un
desarrollo hoy en día eso es imposible. Salvo honrosas excepciones nadie puede abarcar todos los
conocimientos, herramientas y técnicas para desarrollar un producto. El tan manido término "fullstack"
es una quimera. Podrás conocer muchas herramientas de todas las capas pero no podrás dedicarte a todas
ellas pues las soluciones de hoy en día son muchísimo más complejas y requieren de mucho esfuerzo
y dedicación.

A lo largo de todos estos años uno piensa que ha usado muchos lenguajes y herramientas asociadas, y probablemente
es verdad, pero creo que no es tanto la cantidad de lenguajes que hayas usado sino cómo te hayas sentido
identificado con ellos.

Yo, por ejemplo, en un momento dado usé Rexx en un proyecto pero a parte de la satisfacción de que sirviera para lo
que se necesitaba no me siento identificado como programador Rexx (y no porque sea malo).

Por ello, para mí, hay 3 o 4 lenguajes que me marcaron en cada época:

- mis comienzos con C y el momento en que fuí capaz de explicar los punteros (_llegue a dar unos cuantos cursos de C_)
- C++ como paso a eso que se empezaba a conocer como OOP (_aún recuerdo explicando que un objeto es como una librería
y una estructura de datos, todo junto_)
- Java y su posibilidad de emplearlo tanto en el cliente como en el servidor
- Groovy, el cual descubrí casi por casualidad y con el que mi productividad se multiplicó por 1k y por ello
lo uso a diario

Como ya he comentado una etapa (de bastantes años) de mi vida profesional fue como director de una empresa
de servicios que monté junto con un socio. Probablemente lo peor de mi carrera haya venido de cuando una
de tantas crisis me hizo ver que o estaba en una cosa o en otra, pero no en las dos. Así que, tras casi quedar
en quiebra, opté por el rumbo de técnico y a partir de ahí todo ha sido más fácil.

Algunas veces pienso que *lo que más echo de menos de aquella época era la emoción de estar haciendo algo
por donde "nadie había pasado"*. Sin Internet, casi todo te parecía que lo tenías que hacer o inventar tú
(luego descubres que alguien lo había hecho y mejor). Hoy es a la inversa: parece como que todo está inventando
, pero por suerte siempre descubres algo nuevo que te vuelve a traer esa emoción.

== Mayor avance de la industria

Después de más una década como autodidacta decidí sacarme la ingeniería y creo que es donde tomé conciencia de lo
realmente difícil que es hoy en día programar. Tal vez la industria está madurando y especializándose pero también
se está diversificando de una manera increíble y alguien que termina la carrera no es realmente consciente de
la cantidad de herramientas, técnicas y procedimientos que tiene que emplear en el día a día.

Ya no es sólo saber compilar un programa y copiarlo en un sitio. Hay que diseñar y programar test que aseguren
la calidad, hay que trabajar con un equipo usando herramientas colaborativas como git, hacer code reviews,
despliegues continuos, y un sinfín más de pasos.

Pero casi todo esto es producto de la madurez de la profesión y en mi opinión Git es un claro exponente de esto.
De todas las herramientas o lenguajes con los que haya podido trabajar en todo este tiempo, Git ha sido el
mayor cambio que he sufrido.

Podías venir de herramientas como CSV o SVN pero las posibilidades que ofrece Git para que un equipo de trabajo
cree software son inmensas.

== Volver a programar

A pesar de toda esta ingente cantidad de herramientas y lenguajes que parece que hay que emplear para todo,
lo que creo es que se siguen necesitando muchas manos (y alguna cabeza). Cualquiera que se haya dedicado a
esta profesión puede actualizarse sin problema si acude con una mentalidad abierta. Tiene que entender
que las cosas han evolucionado, que no todo es como se hacía antes, probablemente por esa especialización,
pero que en el fondo el asunto sigue siendo el mismo: resolver una necesidad de la forma más adecuada en el momento
. Lo que sí le digo es que tiene que estar dispuesto a aceptar el cambio constante que le caracteriza a esta
profesión

Si hay algo que no debería desanimarte a la hora de optar por ser programador, es la edad. Creo que a diferencia
de otras profesiones, en esta, la gente no siente tanto recelo a personas mayores, pero eso sí tienes que estar
dispuesto a convivir con gente de la que puede que no entiendas sus aficiones y a la que no puedes ver como tus
hijos.

== Por donde empezar

Estoy prácticamente seguro que lo primero que piensa una persona que hace 20 años programara y quisiera retomarlo
sería algo parecido a "esto ha crecido demasiado y ahora hay muchísimas cosas y herramientas que antes no había,
no voy a llegar a entenderlas nunca". Siendo esto cierto, lo que tenemos que ver, es que ya no tenemos que estar
en todas las partes como pasaba antes. Ahora hay más especialización, incluso los mal llamados full-stack no llegan
a cubrir todas las áreas.

No sólo es cuestión de backend vs frontent.

Existen los QA que también tienen que programar test, los Business Analytics que tienen que programar cargas de datos y consultas, etc

Sí creo que hay ciertas herramientas o lenguajes que debes aprender cuanto antes aunque sea a un nivel básico:

- *Git*, es imprescindible. Hace 20 años con suerte el control de versiones era una carpeta compartida.
Hoy en día Git y todo el ecosistema que se ha generado alrededor lo ha cambiado todo.
Es la manera de trabajar con tus compañeros y de no pisarles el trabajo,
así que los conceptos básicos y cierta soltura es imprescindible.

- *Python* (que conste que yo no sé Python) creo que es un lenguaje que no envejece y se está reinventando muy bien.
A día de hoy se está aplicando en muchas áreas diferentes y podrás encontrar una que te guste.
Administración de sistemas, despliegues en la nube, análisis matemático, ...

- Si te gusta más la parte visual y crees HTML y Javascript es sencillo, prepárate porque la explosión que ha
tenido de frameworks, herramientas y áreas es enorme. Te abrumará la cantidad de cosas que hay que aprender
así que lo mejor será acotarlas. Busca un framework pequeño y centrate en él. Hay suficiente demanda como para que
encuentres tu hueco. Yo por ejemplo *no te recomendaría que empezaras con Angular* pues es un monstruo y te puede
sobrepasar. *Comienza con VueJS* por ejemplo, mucho más asequible y con mucho tirón.

En resumen, en mi opinión hay mucha demanda en todas las áreas y no por haber estado 20 años fuera no vas a aportar nada.
Sí creo que necesitarás una formación mínima en un área determinada probablemente de unos cuantos meses (tal vez 6),
es decir, olvidate el "yo lo aprendo por mi cuenta" porque no va a funcionar.

== Conclusión

En verdad es un tema sobre el que llevo los últimos años hablando con algunos managers.

Se centran en los juniors y no saben ver el potencial que aporta alguien que pasa los 40 sin conocimientos de programación.
Madurez, responsabilidad, fidelidad, ganas de mejorar, ...

Probablemente alguien de 45 años y que se está reciclando no vaya a captar todos los detalles (tampoco un Junior lo hace
, lo único que este pueda tener más reciente los estudios o últimas tendencias del mercado), pero probablemente será
alguien que pueda aportar cuidado en el detalle y algo de cabeza crítica.
