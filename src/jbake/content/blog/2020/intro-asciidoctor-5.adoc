= Introducción a Asciidoc y Asciidoctor (5): diagramas
Jorge Aguilera
2020-02-25
:jbake-type: post
:jbake-status: published
:jbake-tags: asciidoc,documentation,write,diagram
:jbake-summary: Breve introducción al lenguaje de marcado asciidoc y su ecosistema, parte 5
:idprefix:

NOTE: Este post es la continuación a link:intro-asciidoctor-4.html[]

WARNING: Yo utilizo sistemas Linux por lo que los comando que indico para instalar software estan orientados a este.
Si tienes interés en saber cómo hacerlo en Windows, escribeme y lo investigaré

== Objetivo

En este post veremos cómo crear diferentes tipos de diagramas usando sólo un editor de texto.

Los tipos de diagramas son variados y de diferentes ámbitos como:

- Diagramas de clases, relaciones, actividades, etc tipicos de UML
- Diagramas de arquitectura software
- Diagramas Gant para organización de proyectos, tareas, equipos, etc
- Representación de componentes UI (textbox, listbox, buttons, checks, etc)
- Gramatica, para representar partes de una sintaxis por ejemplo
- Otros

== Requisitos

El primer requisito que necesitamos es tener instalado  asciidoctor, tal como se ha explicado
en los post anteriores de esta serie.

NOTE: Para este post vamos a usar asciidoctorj

`$sdk man install asciidoctorj`

La instalacion de asciidoctorj incluye por defecto el segundo requisito que necesitaremos para dotar a nuestros documentos
de diagramas, `asciidoctor-diagram`

En caso de que estés usando la versión ruby deberás instalar la gema correspondiente:

`$ gem install asciidoctor-diagram`

En realidad _asciidoctor_diagram_ por si mismo NO genera ningun diagrama, sino que podriamos decir que es el "pegamento"
entre _asciidoctor_ y los diferentes generadores de diagramas. En la página oficial podemos ver los tipos soportados:

    The extensions supports the AsciiToSVG, BlockDiag (BlockDiag, SeqDiag, ActDiag, NwDiag),
    Ditaa, Erd, GraphViz, Mermaid, Msc, PlantUML, Shaape, SvgBob, Syntrax, UMLet, Vega,
    Vega-Lite and WaveDrom syntax.

NOTE: Esta lista no es exclusiva, pudiendo incorporar nuevos interpretes gracias al modelo de extensiones
de Asciidoctor. Yo mismo he hecho alguna extension en Java y es realmente fácil

Si utilizamos el método indicado anteriormente para la instalación de Asciidoctor (AsciidoctorJ) tendremos incluidos
por defecto los diagramas Ditaa y PlantUML (tal vez algun otro) asi que si vamos a usar algun otro tipo tendremos que
tener instalado el ejecutable correspondiente.

De forma generica, y como consejo, yo siempre instalo Graphviz puesto que muchos de estos generadores lo usan

`$sudo apt-get install graphviz`

=== Mermaid

Si por ejemplo queremos generar diagramas usando Mermaid tendremos que ejecutar:

`$ npm install -g mermaid.cli`

o si usamos yarn

`$ yarn global add mermaid.cli`

=== Syntrax

Por su parte, syntrax esta desarrollado en python por lo que tendremos que tener instalado este software y ejecutar

`$pip install --upgrade syntrax`

WARNING: Efectivamente, como has podido adivinar te toca conocer la sintaxis de cada generador que quieras utilizar.
Asciidoctor-diagram solo sirve como puente entre tu documento y el generador

== Sintaxis

La potencia de poder adjuntar a nuestra documentacion diagramas definidos en formato texto es inmensa, pudiendo aprovechar
todas las caracteristicas propias de la filosofía "doc-as-code", es decir, crear y manejar nuestra documentación igual
que lo hacemos con el código: editores de texto, versionable, mergear ramas, aprovaciones, despliegues continuos, etc

=== Include implícito

Asi pues la sintaxis para definir un diagrama a lo largo de nuestra documentación consiste simplemente en crear un
bloque donde se especifique el tipo de diagrama que vamos a usar e incluir el cuerpo del diagrama dentro del mismo:

Por ejemplo:

[source,asciidoctor]
----
Este parrafo es parte de nuestra documentacion, donde queremos poner a continuacion un diagrama de clases

plantuml::fichero_mi_diagrama.txt[]
----

donde _fichero_mi_diagrama.txt_ puede ser un fichero de texto como el siguiente:

[source]
----
class Car

Driver - Car : drives >
Car *- Wheel : have 4 >
Car -- Person : < owns
----

De tal forma que nuestro documento se veria como:

  Este parrafo es parte de nuestra documentación, donde queremos poner a continuación un diagrama de clases

[plantuml]
----
class Car

Driver - Car : drives >
Car *- Wheel : have 4 >
Car -- Person : < owns
----

=== Bloques

Si no quieres crear más ficheros también puedes usar bloques para incluir tus diagramas junto con la documentacion:

[source]
----
Este parrafo es parte de nuestra documentación, donde queremos poner a continuación un diagrama de clases

[plantuml]
++++
class Car

Driver - Car : drives >
Car *- Wheel : have 4 >
Car -- Person : < owns
++++
----

Y el resultado sería el mismo.

== Ejemplos

A continuacion algunos diagramas de ejemplos generados con Ditaa, PlantUML, Mermaid y Syntrax

=== Diseño Software

=== Procesos

[source]
----
[ditaa]
....
                   +-------------+
                   | Asciidoctor |-------+
                   |   diagram   |       |
                   +-------------+       | PNG out
                       ^                 |
                       | ditaa in        |
                       |                 v
 +--------+   +--------+----+    /---------------\
 |        | --+ Asciidoctor +--> |               |
 |  Text  |   +-------------+    |   Beautiful   |
 |Document|   |   !magic!   |    |    Output     |
 |     {d}|   |             |    |               |
 +---+----+   +-------------+    \---------------/
     :                                   ^
     |          Lots of work             |
     +-----------------------------------+
....
----

image::../blog/2020/diagram/ditaa.png[Ditaa box]

=== UML

[source]
----
(incluir complex.txt)
----

image::../blog/2020/diagram/complex.png[Clases]

[source]
----
(incluir parallel.txt)
----

image::../blog/2020/diagram/parallel.png[Activiy Parallel]

[source]
----
(incluir conditional.txt)
----

image::../blog/2020/diagram/conditional.png[Activiy Conditional]

[source]
----
[mermaid]
++++
classDiagram
	Animal <|-- Duck
	Animal <|-- Fish
	Animal <|-- Zebra
	Animal : +int age
	Animal : +String gender
	Animal: +isMammal()
	Animal: +mate()
	class Duck{
		+String beakColor
		+swim()
		+quack()
	}
	class Fish{
		-int sizeInFeet
		-canEat()
	}
	class Zebra{
		+bool is_wild
		+run()
	}
++++
----

image::../blog/2020/diagram/mermaid.png[Clases con Mermaid]

=== Arquitectura

[source]
----
[plantuml]
++++

!define SPRITESURL https://raw.githubusercontent.com/rabelenda/cicon-plantuml-sprites/v1.0/sprites
!includeurl SPRITESURL/tomcat.puml
!includeurl SPRITESURL/kafka.puml
!includeurl SPRITESURL/java.puml
!includeurl SPRITESURL/cassandra.puml
!includeurl SPRITESURL/python.puml
!includeurl SPRITESURL/redis.puml


title Cloudinsight sprites example

skinparam monochrome true

rectangle "<$tomcat>\nwebapp" as webapp
queue "<$kafka>" as kafka
rectangle "<$java>\ndaemon" as daemon
rectangle "<$python>\ndaemon2" as daemon2
database "<$cassandra>" as cassandra
database "<$redis>" as redis

webapp -> kafka
kafka -> daemon
kafka -> daemon2
daemon --> cassandra
daemon2 --> redis
++++
----

image::../blog/2020/diagram/arquitectura.png[Arquitectura software]

=== Gestion proyectos

[source]
----
[mermaid]
++++
gantt
	title A Gantt Diagram
	dateFormat  YYYY-MM-DD
	section Section
	A task           :a1, 2014-01-01, 30d
	Another task     :after a1  , 20d
	section Another
	Task in sec      :2014-01-12  , 12d
	another task      : 24d
++++					
----

image::../blog/2020/diagram/grant.png[Grant]

=== UI/UX

[source]
----
(incluir salt.txt)
----

image::../blog/2020/diagram/salt.png[UI]

image::../blog/2020/diagram/salt2.png[UI]

image::../blog/2020/diagram/salt3.png[UX]

=== Sintaxis

[source]
----
(incluir syntrax1.txt)
----


image::../blog/2020/diagram/syntrax1.png[Railroad]

=== Otros

link:https://gitlab.com/snippets/1779830/raw[Source Tabla periódica]

image::../blog/2020/diagram/tablaperiodica.png[Tabla periodica]




