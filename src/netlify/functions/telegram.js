var request = require('request');

exports.handler = function(event, context, callback) {
    console.log(event.body)
    var obj = JSON.parse(event.body);

    request.post('https://api.telegram.org/bot'+process.env.TELEGRAM_BOT_TOKEN+'/sendMessage', {
        form: {
            "chat_id": process.env.TELEGRAM_CHAT_ID,
            "text": "A user is reading "+obj.url,
            "disable_notification": true
        }
    }, function (error, response, body) {
        console.log('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body);

        callback(null, {
            statusCode: 200,
            body: "Hi! enjoy my blog"
        });
    });

}
