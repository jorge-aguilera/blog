var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');

// spreadsheet key is the long id in the sheets URL
var doc = new GoogleSpreadsheet(process.env.GOOGLE_SHEET);
var sheet;

exports.handler = function(event, context, callback) {
  console.log(event.body)
  var obj = JSON.parse(event.body);
  obj.when = new Date().toISOString();
  
  async.series([
    function setAuth(step) {
      var creds_json = {
        client_email: process.env.GOOGLE_CLIENT_EMAIL,
        private_key: process.env.GOOGLE_PRIVATE_KEY.replace(new RegExp("\\\\n", "\g"), "\n")
      }
      doc.useServiceAccountAuth(creds_json, step);
    },
    function getInfoAndWorksheets(step) {
      doc.getInfo(function(err, info) {
        sheet = info.worksheets[0];
        step();
      });
    },
    function addRow(step) {
      sheet.addRow( obj, function( err, rows ){
        step();
      });
    }
  ], function(err){
      if( err ) {
        console.log('Error: '+err);
      }
      callback(null, {
          statusCode: 200,
          body: "OK"
      });
  });

}
