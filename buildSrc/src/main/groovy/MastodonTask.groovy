import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import groovy.xml.XmlSlurper
import groovy.json.JsonOutput
import java.net.http.*

abstract class MastodonTask extends DefaultTask{

    @Input
    abstract Property<String> getInstance()

    @Input
    abstract Property<String> getToken()

    @Input
    abstract Property<String> getPostId()

    @TaskAction
    def runTask() {
        def xml = new XmlSlurper().parse("https://jorge-aguilera.blog/feed.xml")
        def entry = xml.entry.find{ "$it.id".endsWith( postId.get()+".html")}
        if( !entry ) {
            println("Post no encontrado")
            return
        }
        def hashtags = entry.category.collect{ "#"+it."@term"}.join(' ')
        def toot = """
🗣️ Nuevo post en el blog

$entry.title

📖 ${entry.summary}

$hashtags

${entry.id}
"""
        def json = JsonOutput.toJson([ status: toot])
        println json

        def client = HttpClient.newHttpClient()
        def request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Authorization", "Bearer ${token.get()}")
                .uri(URI.create(instance.get()+'/api/v1/statuses'))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build()
        def response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println response
    }
}

