package jorge.aguilera.soy


import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.*;

import java.util.*;
import java.util.stream.Stream;

@Name("calendar")
@Contexts([Contexts.LISTING])
public class CalendarBlock extends BlockProcessor {

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        Map<String,List<String>>planning = new HashMap<>();

        for( String line : reader.readLines()){
            assert line.split(" ").length > 1;
            String day = line.split(" ")[0];
            String plan = line.substring(line.indexOf(" "));

            if( planning.containsKey(day) == false)
                planning.put(day, new ArrayList<>());

            List<String> lines = planning.get(day);
            lines.add(plan);
        }

        List<String> content = new ArrayList<>();
        if( attributes.containsKey("title"))
            content.add("."+attributes.get("title"));
        content.addAll(Arrays.asList(
                "[cols=a7*,options=header]",
                "|===",
                "|L|M|X|J|V|S|D",
                ""
        ));

        int year = Integer.parseInt(attributes.get("year").toString());
        int month = Integer.parseInt(attributes.get("month").toString());
        month--;
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.set(Calendar.DAY_OF_MONTH,1);
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);

        while( c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            c.add(Calendar.DAY_OF_YEAR,-1);
        }

        while( c.get(Calendar.MONTH) != month){
            content.add("|");
            c.add(Calendar.DATE, 1);
        }

        while( c.get(Calendar.MONTH) == month){
            int d = c.get(Calendar.DAY_OF_MONTH);
            content.add("|"+d);
            if( planning.containsKey(""+d)){
                List<String> plan = planning.get(""+d);
                for(String str : plan){
                    content.add("");
                    content.add(str);
                    content.add("");
                }
            }
            content.add("");
            c.add(Calendar.DATE, 1);
        }

        while( c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            content.add("|");
            c.add(Calendar.DAY_OF_YEAR, 1);
        }

        content.add("|===");

        Block block = createBlock(parent, "open", (String) null);
        parseContent(block, content);
        return block;
    }

}
