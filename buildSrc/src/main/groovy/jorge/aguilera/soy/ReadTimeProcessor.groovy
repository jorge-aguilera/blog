package jorge.aguilera.soy

import org.asciidoctor.ast.Document
import org.asciidoctor.extension.*

import java.util.concurrent.TimeUnit

public class ReadTimeProcessor extends Preprocessor {

    @Override
    public void process(Document document, PreprocessorReader reader){
        List<String> lines = reader.readLines()
        int words = lines.join(' ').split(' ').size()
        int time = Math.round( words / 270)
        document.attributes.put('read_time', convert(time))
        reader.restoreLines(lines)
    }

    String convert(int minutesToConvert) {
        long millis = minutesToConvert * 60_000;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);
        String.format("%02d:%02d", Math.abs(minutes), Math.abs(seconds));        
    }
}