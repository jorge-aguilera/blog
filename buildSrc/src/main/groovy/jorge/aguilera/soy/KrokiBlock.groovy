package jorge.aguilera.soy

import org.asciidoctor.ast.*
import org.asciidoctor.extension.*

import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.optional.Download.*

@Name("kroki")
@Contexts([Contexts.LISTING])
@PositionalAttributes(["type","width","height"])
public class KrokiBlock extends BlockProcessor {

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {        

        def http = configure {
            request.uri = System.getProperty("KROKI_URL",'http://localhost:8000')
            request.contentType = 'text/plain'
            request.accept = 'image/svg+xml'
            request.headers['User-Agent']='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'
        }

        String name = attributes.get("name") ?: "${new Date().time}"
        String extension = "svg"
        File fImageDir = imageDir(parent)
        File fImage = new File(fImageDir, "${name}.${extension}")        
        http.post{
            request.uri = "/"+attributes.get('type')
            request.body = reader.readLines().join('\n')
            toFile(delegate, fImage)
        }
        Map options = [:]
        options.target = imagePath(parent,fImage)
        options.width = attributes.get("width") ?: "800"
        options.height = attributes.get("height") ?: "600"
        createBlock(parent, "image", "", options)
    }

    File imageDir(ContentNode parent){
        File fImageDir=null;

        Object out_dir = parent.getDocument().getAttributes().get("imagesoutdir");

        if( out_dir != null ){
            fImageDir = new File(out_dir.toString());
        }else{
            Object to_dir = parent.getDocument().getOptions().get("to_dir");
            String imagedir = parent.getDocument().getAttributes().get("imagesdir") != null ?
                    parent.getDocument().getAttributes().get("imagesdir").toString() : "images";
            File root = new File(to_dir.toString());
            fImageDir = new File( root, imagedir);
        }
        fImageDir.mkdirs()

        fImageDir
    }

    String imagePath(ContentNode parent, File fImage){
        String imagedir = parent.document.attributes.get("imagesdir") != null ? "." : "images";
        imagedir+"/"+fImage.name
    }

}