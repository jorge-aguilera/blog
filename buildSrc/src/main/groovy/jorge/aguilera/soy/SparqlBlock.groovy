package jorge.aguilera.soy

import groovy.json.*
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

import org.asciidoctor.ast.Block
import org.asciidoctor.ast.StructuralNode
import org.asciidoctor.extension.*

import java.util.*
import java.util.stream.Stream

@Name("sparql")
@Contexts([Contexts.LISTING])
public class SparqlBlock extends BlockProcessor{

    String baseURL(Map<String, Object> attributes){
        switch( attributes.get('provider')){
            case 'dbpedia':
                return "http://dbpedia.org/sparql"
            case 'wikidata':
            case null:
                return "https://query.wikidata.org/sparql"
            default:
                attributes.get('provider')
        }
    }

    String extraQuery(Map<String, Object> attributes){
        if( attributes.get('format'))
            return 'format='+attributes.get('format')
        switch( attributes.get('provider')){
            case 'dbpedia':
                return 'output=json'
            default:
                return 'format=json'
        }        
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        try{
            def fields = attributes.get("fields").split(',').collect{it.trim()} as String[]
            def url = baseURL(attributes)
            def content = executeAndParse(url, fields, reader.readLines(), extraQuery(attributes), attributes.get("title") )

            Block block = createBlock(parent, "open", (String) null);
            parseContent(block, content);
            block
        }catch(e){
            if( attributes.get("failonerror")?.toString() == 'true' ){
                throw e
            }
        }
    }

    List<String> executeAndParse(String url, String[] fields, List<String> query, String extraQuery, String title=null){

        def content = []

        if( title ){
            content.add "."+title
        }

        content.addAll( Arrays.asList(
            "[cols=a${fields.size()}*,options=header]".toString(),
            "|===",
            "|${fields.join('|')}".toString(),
            ""
        ) )

        url = buildURL(url, query, extraQuery)
        
        content.addAll executeAndParseJson(url, fields)
        
        content.add "|==="
        content
    }

    private String buildURL(String url, List<String> query, String extraQuery){
        String queryString = extraQuery+'&'+'query='+URLEncoder.encode( query.join('\n'), StandardCharsets.UTF_8.toString())
        url+'?'+queryString
    }

    List<String> executeAndParseJson(String url, String[] fields){
        
        def resp = url.toURL().text
        
        def jsonSlurper = new JsonSlurper()
        def json = jsonSlurper.parseText(resp)
        
        def content = []

        json.results.bindings.each{ item ->
            content.addAll fields.collect{ 
                String value = item."$it"?.value?.toString() ?: ''
                if( ['jpg','png','gif'].contains(value.toLowerCase().split('\\.').last() ) )
                    value = "image:$value[]"
                "|"+value
            }            
        }

        content
    }
}
