import org.asciidoctor.ast.ContentModel;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockProcessor;
import org.asciidoctor.extension.Contexts;
import org.asciidoctor.extension.Name;
import org.asciidoctor.extension.Reader;

import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.joining;

@Contexts(Contexts.PARAGRAPH)
@ContentModel(ContentModel.COMPOUND)
@Name("yell")
public class YellStaticBlock extends BlockProcessor {

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {

        String upperLines = reader.readLines().collect{ it.toUpperCase() }.join('\n')
            
        createBlock(parent, "paragraph", upperLines, attributes, [:]);
    }
}