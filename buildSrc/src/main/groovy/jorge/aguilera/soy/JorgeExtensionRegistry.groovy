package jorge.aguilera.soy


import org.asciidoctor.Asciidoctor
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry

class JorgeExtensionRegistry implements ExtensionRegistry {
    void register(Asciidoctor asciidoctor) {        
        asciidoctor.javaExtensionRegistry().with{
            block 'calendar', CalendarBlock
            block 'yell', YellStaticBlock
            block 'sparql', SparqlBlock
            block 'kroki', KrokiBlock
            preprocessor ReadTimeProcessor
        }
    }
}