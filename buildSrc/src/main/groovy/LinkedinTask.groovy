import groovy.json.JsonOutput
import groovy.xml.XmlSlurper
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.net.http.*

abstract class LinkedinTask extends DefaultTask{

    @Input
    abstract Property<String> getPublisher()

    @Input
    abstract Property<String> getToken()

    @Input
    abstract Property<String> getPostId()

    @TaskAction
    def runTask() {
        def xml = new XmlSlurper().parse("https://jorge.aguilera.soy/feed.xml")
        def entry = xml.entry.find{ "$it.id".endsWith( postId.get()+".html")}
        if( !entry ) {
            println("Post no encontrado")
            return
        }
        def hashtags = entry.category.collect{ "#"+it."@term"}.join(' ')
        def content = """
🗣️ Nuevo post en mi blog

$entry.title

📖 ${entry.summary}

$hashtags

${entry.id}
"""
        def json = JsonOutput.toJson([
                "author": "urn:li:person:"+publisher.get(),
                "commentary": content,
                "visibility": "PUBLIC",
                "distribution": [
                        "feedDistribution": "MAIN_FEED",
                        "targetEntities": [],
                        "thirdPartyDistributionChannels": [],
                ],
                "lifecycleState": "PUBLISHED",
                "isReshareDisabledByAuthor": false
        ])

        def client = HttpClient.newHttpClient()
        def request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("Authorization", "Bearer ${token.get()}")
                .header('X-Restli-Protocol-Version','2.0.0')
                .header('LinkedIn-Version','202308')
                .uri(URI.create('https://api.linkedin.com/rest/posts'))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build()
        def response = client.send(request, HttpResponse.BodyHandlers.ofString())
        println response
    }
}

